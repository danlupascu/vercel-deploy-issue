module.exports = {
  env: {
    browser: true,
    es2020: true,
    jest: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 11,
    sourceType: 'module',
  },
  plugins: [
    'react',
    '@typescript-eslint',
  ],
  rules: {
    'react/react-in-jsx-scope': 0,
    'react/jsx-filename-extension': [0, { 'extensions': ['.tsx'] }],
    'react/prop-types': 0,
    'react/jsx-props-no-spreading': 0,
    'import/extensions': 0,
    'import/no-unresolved': 0,
    'react/require-default-props': 0,
    'no-underscore-dangle': [
      1,
      {
        allow: ['_id'],
      },
    ],
    // Disable default no unused vars and use the one from the @typescript-eslint
    // https://stackoverflow.com/a/56848207/6744320
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': ['error', { ignoreRestSiblings: true }],

    // Disable default no use before define and use the one from @typescript-eslint
    // https://stackoverflow.com/questions/63818415/react-was-used-before-it-was-defined
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],
    'import/no-extraneous-dependencies': [
      'error',
      {devDependencies: ['**/*.test.ts', '**/*.test.tsx']}
    ],
  },
};
